﻿using System;
using System.Collections.Generic;

namespace Adivinator.Models
{
    public sealed class Juego
    {
        private static readonly Juego instance = new Juego();
        public static List<Jugador> Jugadores { get;  } = new List<Jugador>();
        public static Juego Instance { get { return instance; } }

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static Juego()
        {
        }

        private Juego()
        {
        }

        public Jugador GetJugador(string nombre)
        {
            return Jugadores.Find(j => j.Nombre == nombre);
        }
        
        public static Jugador GetMejor()
        {
            Jugador mejor = null;
            foreach (Jugador jugador in Jugadores)
            {
                if (mejor == null || jugador.Puntos > mejor.Puntos)
                    mejor = jugador;
            }
            return mejor;
        }
                
        public static string Rima()
        {
            Random rand = new Random();
            int num = rand.Next(1, 4);
            switch (num)
            {
                case 1:
                    return "Es un vaivén, del 1 al 100";
                case 2:
                    return "Un centenar, en un decenar";
                case 3:
                    return "1/100 en 1/10";
                default:
                    return "Adivina del 1 al 100";
            }
        }
    }
}