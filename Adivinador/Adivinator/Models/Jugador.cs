﻿namespace Adivinator.Models
{
    public class Jugador
    {
        public string Nombre { get; }
        public int Puntos { get; set; }
        public int PuntosSession { get; set; }
        public Partida Partida { get; set; }
        
        public Jugador (string nombre)
        {
            Nombre = nombre;
        }

        public Jugador()
        {
        }
        
        public void Puntuar(int n)
        {
            PuntosSession += n;
            Puntos += n;
            if (PuntosSession < 0)
                PuntosSession = 0;
            if (Puntos < 0)
                Puntos = 0;
            Partida.IniciarPartida();
        }

        public void Logout()
        {
            PuntosSession = 0;
        }
    }
}