﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Adivinator.Models
{
    public static class CookieHandler
    {
        public static string Serial (string nombre, int puntos)
        {
            return nombre + "{" + puntos + "}";
        }

        public static List<string[]> DeSerial(HttpCookie cookie)
        {
            char[] delim = { '{', '}' };
            List<string[]> cookiestr = new List<string[]>();
            cookiestr.Add(cookie.Value.Split(delim, StringSplitOptions.RemoveEmptyEntries));
            return cookiestr;
        }

        public static HttpCookie Refrescar(HttpCookie cookie)
        {
            List<string[]> cookiestr = DeSerial(cookie);
            cookie.Value = null;
            
            foreach (string[] str in cookiestr)
            {
                for (int i = 0; i < str.Length; i = i + 2)
                {
                    Jugador jugador = Juego.Jugadores.Find(j => j.Nombre == str[i]);
                    if (jugador!=null)
                        cookie.Value += Serial(jugador.Nombre, jugador.Puntos);
                }
            }
            return cookie;
        }

        public static bool Validar (HttpCookie cookie)
        {
            List<string[]> cookiestr = DeSerial(cookie);

            foreach (string[] str in cookiestr)
            {
                for (int i = 0; i < str.Length; i = i + 2)
                {
                    Jugador jugador = Juego.Jugadores.Find(j => j.Nombre == str[i]);
                    if (jugador == null || jugador.Puntos != Convert.ToInt32(str[i+1]))
                        return false;
                }
            }
            return true;
        }

        public static Jugador GetMejor(HttpCookie cookie)
        {
            List<string[]> cookiestr = DeSerial(cookie);
            Jugador mejor = new Jugador();

            foreach (string[] str in cookiestr)
            {
                for (int i = 0; i < str.Length; i = i + 2)
                {
                    Jugador jugador = Juego.Jugadores.Find(j => j.Nombre == str[i]);
                    if (jugador.Puntos > mejor.Puntos)
                        mejor = jugador;
                }
            }
            return mejor;
        }
    }
}