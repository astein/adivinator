﻿using System;
using System.Collections.Generic;

namespace Adivinator.Models
{
    public class Partida
    {
        public int Numero { get; private set; }
        public List<int> Intentos { get; private set; }

        public Partida()
        {
            IniciarPartida();
        }

        public void IniciarPartida()
        {
            Intentos = new List<int>();
            Random random = new Random();
            Numero = random.Next(1, 101);
        }

        public string IntentosToString()
        {
            string intentos = "";
            foreach (var intento in Intentos)
            {
                intentos += intento + " ";
            }
            return intentos;
        }

        public int Adivinar(int n)
        {
            Intentos.Add(n);
            int result = Validar(n);

            if (result == 1)
            {
                Intentos.Remove(n);
            }
            return result;
        }

        public int Validar(int n)
        {
            if (Numero == n)            
                return 1;
            if (Intentos.Count == 10)
                return -1;               

            if (Numero < n)
                return 0;
            if (Numero > n)
                return 2;

            else
                return -2;
        }
    }
}