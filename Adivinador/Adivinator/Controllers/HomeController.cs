﻿using Adivinator.Filters;
using Adivinator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Adivinator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Session["jugador"] != null) { 
                Jugador jugador = (Jugador)Session["jugador"];
                if (jugador.Partida.Intentos.Count > 0)
                    Session["resultado"] = jugador.Partida.Validar(jugador.Partida.Intentos.Last());
                Session["mejor"] = CookieHandler.GetMejor(Request.Cookies["jugadores"]);

            }
            return View();
        }

        [SessionValidator][HttpPost]
        public ActionResult Adivinar(int? numero)
        {
            if (numero.HasValue && numero.Value>0)
            {
                Jugador jugador = (Jugador)Session["jugador"];
                /*try
                {
                    Session["resultado"] = jugador.Partida.Adivinar((int)numero);
                }
                catch (PerdidaException)
                {
                }*/
                Session["resultado"] = jugador.Partida.Adivinar((int)numero);
                if ((int)Session["resultado"] == 1)
                {
                    Session["puntos"] = 10 - jugador.Partida.Intentos.Count;
                    jugador.Puntuar(10 - jugador.Partida.Intentos.Count);
                    HttpCookie cookie = CookieHandler.Refrescar(Request.Cookies["jugadores"]);
                    Response.Cookies.Add(cookie);
                }
                else if((int)Session["resultado"] == -1)
                {
                    Session["era"] = jugador.Partida.Numero;
                    jugador.Puntuar(-10);
                    HttpCookie cookie = CookieHandler.Refrescar(Request.Cookies["jugadores"]);
                    Response.Cookies.Add(cookie);
                }

            }
            else
                Session["resultado"] = -2;
            
            return RedirectToAction("Index");
        }
       
        [HttpPost]
        public ActionResult Login(string nombre)
        {
            if (!String.IsNullOrEmpty(nombre))
            {
                Jugador jugador = Juego.Instance.GetJugador(nombre);
                if (jugador == null)
                {
                    jugador = new Jugador(nombre);
                    Juego.Jugadores.Add(jugador);
                }               
                jugador.Partida = new Partida();
                Session["jugador"] = jugador;

                HttpCookie cookie = Request.Cookies["jugadores"];
                if (cookie == null || !CookieHandler.Validar(cookie))
                {
                    cookie = new HttpCookie("jugadores");
                    cookie.Value = CookieHandler.Serial(jugador.Nombre,jugador.Puntos);
                }

                else
                {
                    List<string[]> cookiestr = CookieHandler.DeSerial(cookie);
                    bool ausente = true;
                    foreach (string[] str in cookiestr)
                    {
                        for(int i = 0; i < str.Length; i = i + 2)
                            if (str[i].Contains(jugador.Nombre))
                            {
                                ausente = false;
                            }
                    }

                    if (ausente)
                    {
                        cookie.Value += CookieHandler.Serial(jugador.Nombre,jugador.Puntos);
                    }
                }
                cookie.Expires = DateTime.Now.AddMonths(1);
                Response.Cookies.Add(cookie);
            }

            return RedirectToAction("Index");
        }
        
        public ActionResult ReiniciarJuego()
        {
            if (Session["jugador"] != null)
            {
                Jugador jugador = (Jugador)Session["jugador"];
                jugador.Puntuar(-jugador.Partida.Intentos.Count);
            }
            HttpCookie cookie = CookieHandler.Refrescar(Request.Cookies["jugadores"]);
            Response.Cookies.Add(cookie);
            Session["resultado"] = null;
            return RedirectToAction("Index");
        }

        public ActionResult Logout()
        {
            if (Session["jugador"] != null) {
                Jugador jugador = (Jugador)Session["jugador"];
                jugador.Puntuar(-jugador.Partida.Intentos.Count);
                jugador.Logout();
            }
            HttpCookie cookie = CookieHandler.Refrescar(Request.Cookies["jugadores"]);
            Response.Cookies.Add(cookie);
            Session.Abandon();
            return RedirectToAction("Index");
        }
    }
}