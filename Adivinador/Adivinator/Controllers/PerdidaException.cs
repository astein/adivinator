﻿using System;
using System.Runtime.Serialization;

namespace Adivinador2.Controllers
{
    [Serializable]
    internal class PerdidaException : Exception
    {
        public PerdidaException()
        {
        }

        public PerdidaException(string message) : base(message)
        {
        }

        public PerdidaException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PerdidaException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}