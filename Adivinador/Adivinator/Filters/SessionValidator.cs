﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Adivinator.Filters
{
    public class SessionValidator : ActionFilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Session["jugador"] == null)
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary { { "Controller", "Home" }, { "Action", "Index" } });
        }
    }
}